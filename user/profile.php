<?php
include $_SERVER['DOCUMENT_ROOT'].'/db_config.php';
/* Este archivo debe manejar la lógica para obtener la información del perfil */

$correo = $_SESSION["correo"];
$tablaUsuarios =    "SELECT usuario.id,usuario.nombre as nombre,usuario.apellido,usuario.correo,
                          usuario.fecha_registro, pais.nombre as pais FROM usuario
                    INNER JOIN pais 
                    ON usuario.pais = pais.cod_pais";
$nombres = array();
$apellidos = array();
$correos = array();
$fecha_registro = array();
$id = 0;
$rs = pg_query( $dbconn, $tablaUsuarios );
    if( $rs )
        {
             if( pg_num_rows($rs) > 0 )
            {
                // Recorrer el resource y mostrar los datos:
                while( $obj = pg_fetch_object($rs) )
                {
                    $nombres[$obj->id] =  $obj->nombre;
                    $apellidos[$obj->id] =  $obj->apellido;
                    $correos[$obj->id] =  $obj->correo;
                    $paises[$obj->id] = $obj->pais;
                    $fecha_registro[$obj->id] =  $obj->fecha_registro;
                }
            }
        }
pg_close($dbconn);
for ($i=1; $i <pg_num_rows($rs) ; $i++) 
{ 
    if($correos[$i] == $correo )
    {
        $id = $i;
        break;
    }
}
?>