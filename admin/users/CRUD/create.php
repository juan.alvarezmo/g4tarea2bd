<?php
/* Este archivo debe manejar la lógica para crear un usuario como admin */
include $_SERVER['DOCUMENT_ROOT'].'/db_config.php';
date_default_timezone_set("America/Santiago");

$name=$_POST['nombre'];
$lastname=$_POST['apellido'];
$email=$_POST['correo'];
$pass=$_POST['password'];

$country=$_POST['pais'];

$columnas="id,nombre,apellido,correo,contraseña,pais,fecha_registro";
$fecha = date('Y-m-d H:i:s');
$admin=0;
//consulta el mayor id y lo aumenta en 1 para el nuevo usuario
$result_id = pg_query_params($dbconn, "SELECT Usuario.id FROM usuario ORDER BY id DESC", array());
$row_id = pg_fetch_assoc($result_id);
$new_id=$row_id['id']+1;

$valores= "$new_id,'$name','$lastname','$email','$pass',$country,'$fecha',$admin";

$registrar="INSERT INTO usuario(id,nombre,apellido,correo,contraseña,pais,fecha_registro,admin) VALUES($valores)";
$sol_reg = pg_query($dbconn,$registrar);

if($sol_reg){
    header('Location:../../../index.html');

}else
    echo " No se pudo añadir el usuario";

pg_close($dbconn);
?>